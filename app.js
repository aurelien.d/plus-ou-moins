// Déclaration des variables

const btn = document.querySelector("#btn");
const reset = document.querySelector("#reset");
const resultat = document.querySelector("#affichageResultat");
let rep = document.querySelector("#reponse");
let random = getRandomArbitrary(0, 100);
let i = 1;
let reponseCorrect;

//Regex pour une valeur en nombre entier entre 0 et 100, on peut aussi utiliser /^(?:[1-9]|[1-9][0-9]|)$/
const regReponse = /^[0-9][0-9]?$|^100(\.\d{0})?$/;

console.log("Nombre à trouver", random);
console.log("Compteur", i)

// Bouton jouer en cliquant et en appuyant sur Entrée

btn.addEventListener("click", (e) => {
    loto();
});
window.addEventListener("keydown", (e) => {
    if (e.keyCode === 13) {
        loto();
    }
});

// Bouton reset

reset.addEventListener("click", function () {
    for (i; i > 0; i--) {
        newP = document.querySelector("#nouveau");
        var parent = resultat;
        parent.removeChild(newP)
    }
    random = getRandomArbitrary(0, 100);
    console.log("reset du nombre à trouver", random);
    i = 1;
    console.log("Compteur remis à ", i)
    document.querySelector("#resultat").innerHTML = "";
    document.querySelector("#bravo").innerHTML = "";
})

// Fonction de vérification et d'affichage des résultats

function loto() {
    reponseCorrect = regReponse.test(rep.value); // Vérification du chiffre rentré
    if (!reponseCorrect) {
        swal("Valeur incorrecte");
    } else {
        if (i <= 10) { // Condition du nombre d'essaies avant la fin du jeu
            if (rep.value != random) { // Condition si la proposition est différente du nombre à trouver
                i++;
                console.log("Compteur " + i)
                if (rep.value < random) {
                    var newP = document.createElement('p');
                    newP.id = 'nouveau';
                    var texte = document.createTextNode(rep.value);
                    newP.appendChild(texte);
                    resultat.appendChild(newP);
                    document.querySelector("#resultat").innerHTML = ("Plus grand que " + rep.value);
                    document.querySelector("#bravo").innerHTML = ("Essai numéro " + i);
                } else if (rep.value > random) {
                    var newP = document.createElement('p');
                    newP.id = 'nouveau';
                    var texte = document.createTextNode(rep.value);
                    newP.appendChild(texte);
                    resultat.appendChild(newP);
                    document.querySelector("#resultat").innerHTML = ("Plus petit que " + rep.value);
                    document.querySelector("#bravo").innerHTML = ("Essai numéro " + i);
                }
            } else if (i == 1) { // Condition si la proposition est égale au nombre à trouver dès le premier essai
                document.querySelector("#resultat").innerHTML = "Bravo";
                document.querySelector("#bravo").innerHTML = "Du premier coup !";
            } else {
                var newP = document.createElement('p');
                newP.id = 'nouveau';
                var texte = document.createTextNode(rep.value);
                newP.appendChild(texte);
                resultat.appendChild(newP);
                document.querySelector("#resultat").innerHTML = "Bravo ouais youpi tralala trop fort";
                document.querySelector("#bravo").innerHTML = "Trouvé en " + i + " essais !";
            }
        } else {
            document.querySelector("#resultat").innerHTML = "Et c'est l'échec";
            document.querySelector("#bravo").innerHTML = "Réessaye une autre fois";
        }
    }
    document.querySelector("#reponse").value = ""; // Vide l'input de sa valeur
    document.querySelector("#reponse").focus(); // Focus sur l'input number
}

// Fonction géérant un random

function getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}