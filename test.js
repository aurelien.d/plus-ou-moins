// Déclaration des variables

const btn = document.querySelector("#btn");
const reset = document.querySelector("#reset");
let rep = document.querySelector("#reponse");
let random = getRandomArbitrary(0, 100);
let i = 1;

let reponseCorrect;

//Regex pour une valeur en nombre entier entre 0 et 100, on peut aussi utiliser /^(?:[1-9]|[1-9][0-9]|)$/
const regReponse = /^[1-9][0-9]?$|^100(\.\d{0})?$/;

// window.event.keyCode == 13 // Touche entrée

console.log("Nombre à trouver", random);
console.log("Compteur", i)
// Bouton reset

reset.addEventListener("click", function () {
    random = getRandomArbitrary(0, 100);
    console.log("Reset du nombre à trouver", random);
    i = 1;
    console.log("Reset compteur", i)
    document.querySelector("#affichage").innerHTML = "";
    document.querySelector("#nbEssai").innerHTML = "";
})

// Bouton jouer en utilisant le clic

btn.addEventListener("click", function (e) {
    reponseCorrect = regReponse.test(rep.value); // Vérification du chiffre rentré
    console.log(reponseCorrect);
    if (!reponseCorrect) {
        document.querySelector("#affichage").innerHTML = "Valeur incorrecte";
    } else {
        if (i <= 10) { // Condition du nombre d'essaies avant la fin du jeu
            if (rep.value != random) { // Condition si la proposition est différente du nombre à trouver
                i++;

                var newP= document.createElement('p');
                newP.id = 'nouveau';
                var texte = document.createTextNode("Essai"+ i );
                newP.appendChild(texte);
                document.body.appendChild(newP);

                document.querySelector("#nbEssai").innerHTML = "Essai numéro " + i;
                console.log(i)
                if (rep.value < random) {
                    document.querySelector("#affichage").innerHTML = "Plus grand";
                } else if (rep.value > random) {
                    document.querySelector("#affichage").innerHTML = "Plus petit";
                }
            } else if (i == 1) { // Condition si la proposition est égale au nombre à trouver dès le premier essai
                document.querySelector("#affichage").innerHTML = "Bravo";
                document.querySelector("#nbEssai").innerHTML = "Du premier coup !";
            } else {
                document.querySelector("#affichage").innerHTML = "Bravo";
                document.querySelector("#nbEssai").innerHTML = "Trouvé en " + i + " essais !";
            }
        } else {
            document.querySelector("#affichage").innerHTML = "Et c'est l'échec";
        }
    }
    document.querySelector("#reponse").value = ""; // Vide l'input de sa valeur
    document.querySelector("#reponse").focus(); // Focus sur l'input number
})

// Bouton jouer en utilisant Entrée

window.addEventListener("keydown", function (e) {
    if (e.keyCode == 13) {

        reponseCorrect = regReponse.test(rep.value); // Vérification du chiffre rentré
        console.log(reponseCorrect);
        if (!reponseCorrect) {
            document.querySelector("#affichage").innerHTML = "Valeur incorrecte";
        } else {
            if (i <= 10) { // Condition du nombre d'essaies avant la fin du jeu
                if (rep.value != random) { // Condition si la proposition est différente du nombre à trouver
                    i++;
                    document.querySelector("#nbEssai").innerHTML = "Essai numéro " + i;
                    console.log(i)
                    if (rep.value < random) {
                        document.querySelector("#affichage").innerHTML = "Plus grand";
                    } else if (rep.value > random) {
                        document.querySelector("#affichage").innerHTML = "Plus petit";
                    }
                } else if (i == 1) { // Condition si la proposition est égale au nombre à trouver dès le premier essai
                    document.querySelector("#affichage").innerHTML = "Bravo";
                    document.querySelector("#nbEssai").innerHTML = "Du premier coup !";
                } else {
                    document.querySelector("#affichage").innerHTML = "Bravo";
                    document.querySelector("#nbEssai").innerHTML = "Trouvé en " + i + " essais !";
                }

            } else {
                document.querySelector("#affichage").innerHTML = "Et c'est l'échec";
            }
        }
        document.querySelector("#reponse").value = ""; // Vide l'input de sa valeur
        document.querySelector("#reponse").focus(); // Focus sur l'input number
    }
})



function getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function game(event) {

}

function essai(){
    var newP= document.createElement('p');
    newP.id = 'nouveau';
    var texte = document.createTextNode("Essai"+ i );
    newP.appendChild(texte);
    document.body.appendChild(newP);
}


